import { IRepository } from 'utils/utils.interfaces';

// Entities
export interface IUser {
  id: number;
  username: string;
  avatar: string;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  encryptPassword(password?: string): void;
  comparePassword(password: string): boolean;
}

// Repositories
export interface IUsersRepository extends IRepository<IUser> {
  findByUsername(username: string): Promise<IUser>;
}
