import { compareSync, hashSync } from 'bcrypt';
import { Exclude } from 'class-transformer';
import { IUser } from 'databases/databases.interfaces';
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: 'users',
})
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column({
    nullable: true,
  })
  avatar: string;

  @Column()
  @Exclude()
  password: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt?: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
  })
  deletedAt?: Date;

  @BeforeInsert()
  encryptPassword(password?: string) {
    this.password = hashSync(password || this.password, 10);
  }

  comparePassword(password: string) {
    return compareSync(password, this.password);
  }
}
