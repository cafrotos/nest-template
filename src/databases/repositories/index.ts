import { readRecursiveDirSync } from 'utils';
import * as _ from 'lodash';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';

export type RepositoryName = 'Users';

export const repositories = _.flatMap(
  readRecursiveDirSync(__dirname, [/.+\.repository\.(ts|js)$/g]).map(
    (path) =>
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      Object.values(require(path)) as EntityClassOrSchema[],
  ),
).filter((repository: any) => repository.name.match(/.+Repository$/g));

export const getRepositoryForFeature = (
  repositoryNames?: Array<RepositoryName>,
  connectionName?: string,
) => {
  if (!repositoryNames || repositoryNames.length === 0) {
    return TypeOrmModule.forFeature(
      repositories.filter((repository: any) =>
        repositoryNames.includes(repository.name.replace('Repository', '')),
      ),
      connectionName,
    );
  }
  return TypeOrmModule.forFeature(repositories, connectionName);
};

export const InjectRepo = (repoName: RepositoryName, connection?: string) => {
  return (
    target: any,
    propertyKey: string | symbol,
    parameterIndex: number,
  ) => {
    InjectRepository(
      repositories.find(
        (repository: any) =>
          repository.name.replace('Repository', '') === repoName,
      ),
      connection,
    )(target, propertyKey, parameterIndex);
  };
};
