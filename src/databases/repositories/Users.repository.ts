import { IUsersRepository } from 'databases/databases.interfaces';
import { UserQueryTypeORM } from 'databases/dto/UserQueryTypeORM.dto';
import { User } from 'databases/entities/User';
import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'utils/bases/Repository.base';
import { IQuery } from 'utils/utils.interfaces';

@EntityRepository(User)
export class UsersRepository
  extends BaseRepository<User>
  implements IUsersRepository
{
  findByUsername(username: string) {
    return this.createQueryBuilder('users')
      .where('username = :username', { username })
      .innerJoinAndSelect('users.role', 'roles')
      .leftJoinAndSelect('roles.permissions', 'permissions')
      .getOne();
  }

  getQueryBuilderList(query: IQuery) {
    return super.getQueryBuilderList(
      UserQueryTypeORM.createQueryTypeORM(query),
      'users',
    );
  }
}
