import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { template } from 'configs/databases';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...template,
      entities: [__dirname + '/entities/*{.ts,.js}'],
    }),
  ],
})
export class DatabasesModule {}
