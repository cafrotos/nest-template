import { User } from 'databases/entities/User';
import { SelectQueryBuilder } from 'typeorm';
import { BaseQueryTypeORM } from 'utils/bases/Query';

export class UserQueryTypeORM extends BaseQueryTypeORM {
  username: string;
  ids: Array<number | string>;

  usernameWithQueryBuilder(queryBuilder: SelectQueryBuilder<User>) {
    return queryBuilder.andWhere(`${queryBuilder.alias}.username = :username`, {
      username: this.username,
    });
  }

  idsWithQueryBuilder(queryBuilder: SelectQueryBuilder<User>) {
    return queryBuilder.andWhere(`${queryBuilder.alias}.id in (:ids)`, {
      ids: this.ids,
    });
  }
}
