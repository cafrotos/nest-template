import { JwtSignOptions, JwtVerifyOptions } from '@nestjs/jwt';
import { getContentFile } from 'utils';

export const sign = {
  accessToken: <JwtSignOptions>{
    privateKey: getContentFile('keys/accessTokenPrivateKey.pem'),
    algorithm: 'RS256',
    expiresIn: '1h',
  },
  refreshToken: <JwtSignOptions>{
    privateKey: getContentFile('keys/refreshTokenPrivateKey.pem'),
    algorithm: 'RS256',
    expiresIn: '1d',
  },
  downdloadToken: <JwtSignOptions>{
    secret: process.env.DOWNLOAD_SECRET_KEY || 'D04n104d_s3c3t_k3y',
    algorithm: 'HS256',
    expiresIn: '5m',
  },
};

export const verify = {
  accessToken: <JwtVerifyOptions>{
    publicKey: getContentFile('keys/accessTokenPublicKey.pem'),
  },
  refreshToken: <JwtVerifyOptions>{
    publicKey: getContentFile('keys/refreshTokenPublicKey.pem'),
  },
  downloadToken: <JwtVerifyOptions>{
    secret: sign.downdloadToken.secret,
  },
};
