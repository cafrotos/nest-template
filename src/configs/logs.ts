import { DailyRotateFileTransportOptions } from 'winston-daily-rotate-file';

export const dailyLogs: DailyRotateFileTransportOptions = {
  filename: process.env.LOGS_PATTERN || '%DATE%.log',
  dirname: process.env.LOGS_PATH || 'logs',
  level: process.env.LOGS_LEVEL || 'info',
  handleExceptions: true,
  json: false,
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '14d',
};
