import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const template: TypeOrmModuleOptions = {
  type: 'mysql',
  host: process.env.DB_HOST || 'localhost',
  port: +(process.env.DB_PORT || '3306'),
  username: process.env.DB_USERNAME || 'root',
  password: process.env.DB_PASSWORD || 'root',
  database: process.env.DB_NAME || 'template',
  synchronize: true,
  logger: 'simple-console',
  logging: 'all',
};
