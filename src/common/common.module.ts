import { CacheModule, Global, Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { getRepositoryForFeature } from 'databases/repositories';
import { AllExceptionFilter } from './filters/AllExceptionFilter';
import { services } from './services';

@Global()
@Module({
  imports: [
    JwtModule.register({}),
    CacheModule.register(),
    getRepositoryForFeature(['Users']),
  ],
  providers: [
    ...services,
    {
      provide: APP_FILTER,
      useClass: AllExceptionFilter,
    },
  ],
  exports: services,
})
export class CommonModule {}
