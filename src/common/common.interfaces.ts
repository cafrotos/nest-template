import { Ability } from '@casl/ability';
import { Action, Subjects } from 'configs/permissions';
import { JwtPayload } from './dto/JwtPayload.dto';
import { JwtPayloadDownload } from './dto/JwtPayloadDownload.dto';

export enum CommonProvider {
  CacheService = 'CacheService',
  CaslService = 'CaslService',
  JwtService = 'JwtService',
  LoggerService = 'LoggerService',
}

export interface ICacheService {
  findOrCreateByName<T>(
    name: string,
    getCacheValue: () => T | Promise<T>,
  ): Promise<T>;
}

export interface ICaslService {
  createAbility(user: JwtPayload): Promise<Ability<[Action, Subjects]>>;
}

export interface IJwtService {
  // Generate and verify user token
  signAccessToken(payload: JwtPayload): string;
  signRefreshToken(payload: JwtPayload): string;
  verifyAccessToken(token: string): JwtPayload;
  verifyRefreshToken(token: string): JwtPayload;

  // Generate and verify download token
  signDownloadToken(payload: JwtPayloadDownload): string;
}

export interface ILoggerService {
  error(message: any, trace?: string, context?: string): void;
  warn(message: any, context?: string): void;
  log(message: any, context?: string): void;
  debug(message: any, context?: string): void;
  verbose(message: any, context?: string): void;
}
