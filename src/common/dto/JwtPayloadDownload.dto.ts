export class JwtPayloadDownload {
  user: {
    id: number;
    username: string;
  };
}
