import { Ability } from '@casl/ability';

export class JwtPayload {
  user: {
    id: number;
    username: string;
  };
  role: {
    name: string;
    permissions: string[];
  };
  ability?: Ability;
}
