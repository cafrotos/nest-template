import { SetMetadata, UseGuards } from '@nestjs/common';
import { applyDecorators } from '@nestjs/common';
import { PoliciesGuard } from 'common/guards/permissions.guard';
import { AppAbility } from 'common/services/casl.service';
import {
  JwtAuthGuard,
  JwtDownloadGuard,
} from 'common/services/jwt.strategy.service';

interface IPolicyHandler {
  handle(ability: AppAbility): boolean;
}

type PolicyHandlerCallback = (ability: AppAbility) => boolean;

export type PolicyHandler = IPolicyHandler | PolicyHandlerCallback;

export const CHECK_POLICIES_KEY = 'check_policy';

export const Auth = (...handlers: PolicyHandler[]) => {
  if (!handlers.length) {
    handlers.push(() => true);
  }
  return applyDecorators(
    UseGuards(JwtAuthGuard, PoliciesGuard),
    SetMetadata(CHECK_POLICIES_KEY, handlers),
  );
};

export const Downloadable = (...handlers: PolicyHandler[]) => {
  if (!handlers.length) {
    handlers.push(() => true);
  }
  return applyDecorators(
    UseGuards(JwtDownloadGuard, PoliciesGuard),
    SetMetadata(CHECK_POLICIES_KEY, handlers),
  );
};
