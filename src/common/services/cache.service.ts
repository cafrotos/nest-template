import { CACHE_MANAGER, Inject, Injectable, Provider } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { CommonProvider, ICacheService } from 'common/common.interfaces';

@Injectable()
class CacheService implements ICacheService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  async findOrCreateByName<T>(
    name: string,
    getCacheValue: () => T | Promise<T>,
  ): Promise<T> {
    const existCached = await this.cacheManager.get<T>(name);

    if (existCached) {
      return existCached;
    }

    const cacheValue = await getCacheValue();
    await this.cacheManager.set(name, cacheValue);

    return cacheValue;
  }
}

export const provideCacheService: Provider = {
  provide: CommonProvider.CacheService,
  useClass: CacheService,
};
