import { Strategy } from '@cafrotos/passport-local';
import { AuthGuard, PassportStrategy } from '@nestjs/passport';
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from 'databases/entities/User';
import { CommonProvider, IJwtService } from 'common/common.interfaces';
import { IUsersRepository } from 'databases/databases.interfaces';
import { InjectRepo } from 'databases/repositories';

@Injectable()
export class LocalStrategyService extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepo('Users')
    private usersRepository: IUsersRepository,
    @Inject(CommonProvider.JwtService) private jwtService: IJwtService,
  ) {
    super();
  }

  async validate(
    username: string,
    password: string,
    refreshToken: string,
  ): Promise<User> {
    if (refreshToken) {
      try {
        const payload = this.jwtService.verifyRefreshToken(refreshToken);
        username = payload.user?.username;
      } catch (error) {
        throw new UnauthorizedException();
      }
    }
    const user = await this.usersRepository.findByUsername(username);

    // Chấp nhận check tồn tại refreshToken do đến bước này refreshToken đã được verify
    if (!user || (!refreshToken && !user.comparePassword(password))) {
      throw new UnauthorizedException();
    }
    return user;
  }
}

export class LocalAuthGuard extends AuthGuard('local') {}
