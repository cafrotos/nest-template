import { Injectable, Provider } from '@nestjs/common';
import { JwtService as BaseJwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { CommonProvider, IJwtService } from 'common/common.interfaces';
import { JwtPayload } from 'common/dto/JwtPayload.dto';
import { JwtPayloadDownload } from 'common/dto/JwtPayloadDownload.dto';
import { sign, verify } from 'configs/jwt';

@Injectable()
class JwtService implements IJwtService {
  constructor(private baseJwtService: BaseJwtService) {}

  // Generate and verify user token
  signAccessToken(payload: JwtPayload) {
    return this.baseJwtService.sign(
      Object.assign({}, payload),
      sign.accessToken,
    );
  }

  signRefreshToken(payload: JwtPayload) {
    return this.baseJwtService.sign(
      Object.assign({}, payload),
      sign.refreshToken,
    );
  }

  verifyAccessToken(token: string): JwtPayload {
    const payload = this.baseJwtService.verify(token, verify.accessToken);
    return plainToClass(JwtPayload, payload);
  }

  verifyRefreshToken(token: string): JwtPayload {
    const payload = this.baseJwtService.verify(token, verify.refreshToken);
    return plainToClass(JwtPayload, payload);
  }

  // Generate and verify download token
  signDownloadToken(payload: JwtPayloadDownload) {
    return this.baseJwtService.sign(
      Object.assign({}, payload),
      sign.downdloadToken,
    );
  }

  verifyDownloadToken(token: string): JwtPayloadDownload {
    const payload = this.baseJwtService.verify(token, verify.refreshToken);
    return plainToClass(JwtPayloadDownload, payload);
  }
}

export const provideJwtService: Provider = {
  provide: CommonProvider.JwtService,
  useClass: JwtService,
};
