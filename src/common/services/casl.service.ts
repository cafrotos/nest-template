import {
  Ability,
  ExtractSubjectType,
  MongoQuery,
  SubjectRawRule,
} from '@casl/ability';
import { AnyObject } from '@casl/ability/dist/types/types';
import { Inject, Injectable, Provider } from '@nestjs/common';
import {
  CommonProvider,
  ICacheService,
  ICaslService,
} from 'common/common.interfaces';
import { JwtPayload } from 'common/dto/JwtPayload.dto';
import { Action, Subjects } from 'configs/permissions';

export type AppSubjectRawRule = SubjectRawRule<
  Action,
  ExtractSubjectType<Subjects>,
  MongoQuery<AnyObject>
>;

export type AppAbility = Ability<[Action, Subjects]>;

@Injectable()
class CaslService implements ICaslService {
  constructor(
    @Inject(CommonProvider.CacheService) private cacheService: ICacheService,
  ) {}

  async createAbility(user: JwtPayload): Promise<Ability<[Action, Subjects]>> {
    const permissions = await Promise.all(
      user.role.permissions.map((permissionName) =>
        /**
         * @Todo implement permissions
         *  */
        this.cacheService.findOrCreateByName<any>(permissionName, () => ({})),
      ),
    );

    return new Ability<[Action, Subjects]>(permissions as AppSubjectRawRule[]);
  }
}

export const provideCaslService: Provider = {
  provide: CommonProvider.CaslService,
  useClass: CaslService,
};
