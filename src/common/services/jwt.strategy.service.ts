import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthGuard, PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { verify } from 'configs/jwt';
import { plainToClass } from 'class-transformer';
import { JwtPayloadDownload } from 'common/dto/JwtPayloadDownload.dto';
import { JwtPayload } from 'common/dto/JwtPayload.dto';

@Injectable()
export class JwtUserStrategyService extends PassportStrategy(
  Strategy,
  'jwt-verify-user',
) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: verify.accessToken.publicKey,
    });
  }

  async validate(payload) {
    return plainToClass(JwtPayload, payload);
  }
}

@Injectable()
export class JwtDownloadStrategyService extends PassportStrategy(
  Strategy,
  'jwt-verify-download',
) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromUrlQueryParameter('token'),
      ignoreExpiration: false,
      secretOrKey: verify.downloadToken.secret,
    });
  }

  async validate(payload) {
    return plainToClass(JwtPayloadDownload, payload);
  }
}

export class JwtAuthGuard extends AuthGuard('jwt-verify-user') {}

export class JwtDownloadGuard extends AuthGuard('jwt-verify-download') {}
