import { Injectable, Logger, Provider, Scope } from '@nestjs/common';
import { CommonProvider, ILoggerService } from 'common/common.interfaces';
import { dailyLogs } from 'configs/logs';
import * as path from 'path';
import * as winston from 'winston';
import 'winston-daily-rotate-file';

const getLogDebug = (log: unknown) => {
  if (log instanceof Error) {
    return log.stack;
  }
  return JSON.stringify(log);
};

const logger = winston.createLogger({
  level: process.env.LOG_LEVEL
    ? process.env.LOG_LEVEL
    : process.env.NODE_ENV === 'production'
    ? 'info'
    : 'debug',
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.timestamp(),
    winston.format.printf((log: any) => {
      if (log.stack) {
        return `[${log.timestamp}] [${log.level}]: ${
          log.debug ? getLogDebug(log.debug) : ''
        } \n  ${log.stack}`;
      }
      return `[${log.timestamp}] [${log.level}]: ${JSON.stringify(
        log.message,
      )}`;
    }),
  ),
});

if (process.env.LOG_LEVEL) {
  logger.add(
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
    }),
  );
}
switch (process.env.NODE_ENV) {
  case 'production':
    logger.add(
      new winston.transports.DailyRotateFile({
        level: 'info',
        filename: path.join(dailyLogs.dirname, '%DATE%.log'),
        zippedArchive: true,
        maxFiles: '14d',
        maxSize: '50MB',
      }),
    );
    break;
  default:
    logger.add(
      new winston.transports.DailyRotateFile({
        level: 'debug',
        filename: path.join(dailyLogs.dirname, '%DATE%.log'),
        zippedArchive: true,
        maxFiles: '3d',
        maxSize: '50MB',
      }),
    );
    break;
}

@Injectable({ scope: Scope.TRANSIENT })
class LoggerService extends Logger implements ILoggerService {
  error(log: any) {
    logger.error(log);
    super.error(log, log.stack, log.context);
  }

  warn(log: any) {
    logger.warn(log);
    super.warn(log);
  }

  log(log: any) {
    logger.log(log);
    super.log(log);
  }

  debug(log: any) {
    logger.debug(log);
    super.debug(log);
  }

  verbose(log: any) {
    logger.verbose(log);
    super.verbose(log);
  }
}

export const provideLoggerService: Provider = {
  provide: CommonProvider.LoggerService,
  useClass: LoggerService,
};
