import { DynamicModule, Module } from '@nestjs/common';
import { readRecursiveDirSync } from 'utils';
import * as _ from 'lodash';

const imports = _.flatMap(
  readRecursiveDirSync(__dirname, [/^((?!resource).)*\.module\.(ts|js)$/g]).map(
    (path) =>
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      Object.values(require(path)) as DynamicModule[],
  ),
);

@Module({
  imports,
})
export class ResourcesModule {}
