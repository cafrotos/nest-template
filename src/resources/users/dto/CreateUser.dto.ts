import { Expose } from 'class-transformer';

export class CreateUserDto {
  @Expose()
  username: string;

  @Expose()
  avatar: string;

  @Expose()
  password: string;
}
