import { Expose } from 'class-transformer';
import { BaseQuery } from 'utils/bases/Query';

export class DeleteUsersDto extends BaseQuery {
  @Expose()
  ids: Array<number | string>;
}
