import { Expose } from 'class-transformer';
import { BaseQuery } from 'utils/bases/Query';

export class GetUsersDto extends BaseQuery {
  @Expose()
  username: string;
}
