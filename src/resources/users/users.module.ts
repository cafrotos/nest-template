import { Module } from '@nestjs/common';
import { getRepositoryForFeature } from 'databases/repositories';
import { services } from './services';
import { controllers } from './controllers';

@Module({
  imports: [getRepositoryForFeature(['Users'])],
  controllers,
  providers: services,
})
export class UsersModule {}
