import { HttpException, Injectable, Provider } from '@nestjs/common';
import { IUser, IUsersRepository } from 'databases/databases.interfaces';
import { InjectRepo } from 'databases/repositories';
import { ResponseBuilder } from 'utils/bases/ResponseBuilder';
import { CreateUserDto } from '../dto/CreateUser.dto';
import { DeleteUsersDto } from '../dto/DeleteUses.dto';
import { GetUsersDto } from '../dto/GetUsers.dto';
import { UpdateUserDto } from '../dto/UpdateUser.dto';
import { ICRUDUserService, UsersProvider } from '../users.interfaces';

@Injectable()
class CRUDUserService implements ICRUDUserService {
  constructor(@InjectRepo('Users') private usersRepo: IUsersRepository) {}
  async create(createUserDto: CreateUserDto) {
    const user = this.usersRepo.create(createUserDto);

    await this.usersRepo.save(user);

    return new ResponseBuilder<IUser>().withDataSource(user);
  }

  async findAll(getUsersDto: GetUsersDto) {
    const [users, total] = await this.usersRepo
      .getQueryBuilderList(getUsersDto, 'users')
      .getManyAndCount();

    return new ResponseBuilder<IUser[]>()
      .withDataSource(users)
      .withPage(getUsersDto.page)
      .withPageSize(getUsersDto.pageSize)
      .withTotal(total);
  }

  async findOne(id: number) {
    const [user] = await this.usersRepo.findByIds([id]);
    return new ResponseBuilder<IUser>().withDataSource(user);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const [user] = await this.usersRepo.findByIds([id]);

    if (!user) {
      throw new HttpException('User not found!', 404);
    }

    Object.keys(updateUserDto).map((key) => {
      user[key] = updateUserDto[key];
      return key;
    });

    await this.usersRepo.save(user);

    return new ResponseBuilder<IUser>().withDataSource(user);
  }

  async remove(deleteUsersDto: DeleteUsersDto) {
    await this.usersRepo
      .getQueryBuilderList(deleteUsersDto, 'users')
      .orderBy(null)
      .limit(null)
      .offset(null)
      .softDelete()
      .execute();
  }
}

export const provideCRUDService: Provider = {
  provide: UsersProvider.CRUDUserService,
  useClass: CRUDUserService,
};
