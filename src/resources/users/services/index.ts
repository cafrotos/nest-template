import { Provider } from '@nestjs/common';
import { readRecursiveDirSync } from 'utils';
import * as _ from 'lodash';

export const services = _.flatMap(
  readRecursiveDirSync(__dirname, [/.+\.service\.(ts|js)$/g]).map(
    (path) =>
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      Object.values(require(path)) as Provider[],
  ),
).filter((provider: any) =>
  (provider.name || provider.provide).match(/.+Service$/g),
);
