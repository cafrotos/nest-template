import { readRecursiveDirSync } from 'utils';
import * as _ from 'lodash';
import { Type } from '@nestjs/common';

export const controllers = _.flatMap(
  readRecursiveDirSync(__dirname, [/.+\.controller\.(ts|js)$/g]).map(
    (path) =>
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      Object.values(require(path)) as unknown as Type<any>,
  ),
).filter((provider: any) =>
  (provider.name || provider.provide).match(/.+Controller$/g),
);
