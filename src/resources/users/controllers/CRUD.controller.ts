import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Inject,
} from '@nestjs/common';
import { CreateUserDto } from '../dto/CreateUser.dto';
import { DeleteUsersDto } from '../dto/DeleteUses.dto';
import { GetUsersDto } from '../dto/GetUsers.dto';
import { UpdateUserDto } from '../dto/UpdateUser.dto';
import { ICRUDUserService, UsersProvider } from '../users.interfaces';

@Controller({
  path: 'users',
  version: '1',
})
export class CRUDUsersController {
  constructor(
    @Inject(UsersProvider.CRUDUserService)
    private readonly crudUserService: ICRUDUserService,
  ) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.crudUserService.create(createUserDto);
  }

  @Get()
  findAll(@Query() getUsersDto: GetUsersDto) {
    return this.crudUserService.findAll(getUsersDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.crudUserService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.crudUserService.update(+id, updateUserDto);
  }

  @Delete()
  remove(@Body() deleteUsersDto: DeleteUsersDto) {
    return this.crudUserService.remove(deleteUsersDto);
  }
}
