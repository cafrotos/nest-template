import { IUser } from 'databases/databases.interfaces';
import { IResponseBuilder } from 'utils/utils.interfaces';
import { CreateUserDto } from './dto/CreateUser.dto';
import { DeleteUsersDto } from './dto/DeleteUses.dto';
import { GetUsersDto } from './dto/GetUsers.dto';
import { UpdateUserDto } from './dto/UpdateUser.dto';

export enum UsersProvider {
  CRUDUserService = 'CRUDUserService',
}

export interface ICRUDUserService {
  create(createUserDto: CreateUserDto): Promise<IResponseBuilder<IUser>>;
  findAll(getUsersDto: GetUsersDto): Promise<IResponseBuilder<IUser[]>>;
  findOne(id: number): Promise<IResponseBuilder<IUser>>;
  update(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<IResponseBuilder<IUser>>;
  remove(deleteUsersDto: DeleteUsersDto): Promise<void>;
}
