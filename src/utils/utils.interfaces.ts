import { Repository, SelectQueryBuilder } from 'typeorm';

export interface IQuery {
  page: number;
  pageSize: number;
  createdBy: number;
}

export interface IQueryTypeORM extends IQuery {
  toQueryBuilder(
    queryBuilder: SelectQueryBuilder<any>,
  ): SelectQueryBuilder<any>;
}

export interface IRepository<T = any> extends Repository<T> {
  getQueryBuilderList(query: IQuery, alias: string): SelectQueryBuilder<T>;
}

export interface IResponseBuilder<T> {
  dataSource: T;
  page?: number;
  pageSize?: number;
  total?: number;

  withDataSource(dataSource: T): IResponseBuilder<T>;
  withPage(page: number): IResponseBuilder<T>;
  withPageSize(pageSize: number): IResponseBuilder<T>;
  withTotal(pageSize: number): IResponseBuilder<T>;
}
