import { existsSync, readdirSync, readFileSync, statSync } from 'fs';

export const readRecursiveDirSync = (
  dir: string,
  patterns: Array<string | RegExp>,
  filenames: string[] = [],
) => {
  readdirSync(dir).map((filename) => {
    if (statSync(`${dir}/${filename}`).isDirectory()) {
      readRecursiveDirSync(`${dir}/${filename}`, patterns, filenames);
    }
    if (
      [...patterns].filter((pattern) => {
        if (typeof pattern === 'string') {
          return pattern === filename;
        }
        return pattern.test(filename);
      }).length > 0
    ) {
      filenames.push(`${dir}/${filename}`);
    }
    return filename;
  });
  return filenames;
};

export const getContentFile = (filename: string) => {
  if (!existsSync(filename)) {
    throw Error('Filename not found: ' + filename);
  }
  return readFileSync(filename, {
    encoding: 'utf-8',
  });
};
