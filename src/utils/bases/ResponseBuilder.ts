import { IResponseBuilder } from 'utils/utils.interfaces';

export class ResponseBuilder<T> implements IResponseBuilder<T> {
  dataSource: T;
  page?: number;
  pageSize?: number;
  total?: number;

  withDataSource(dataSource: T): IResponseBuilder<T> {
    this.dataSource = dataSource;
    return this;
  }
  withPage(page: number): IResponseBuilder<T> {
    this.page = page;
    return this;
  }
  withPageSize(pageSize: number): IResponseBuilder<T> {
    this.pageSize = pageSize;
    return this;
  }
  withTotal(total: number): IResponseBuilder<T> {
    this.total = total;
    return this;
  }
}
