import { Expose, Transform } from 'class-transformer';
import { SelectQueryBuilder } from 'typeorm';
import { IQuery, IQueryTypeORM } from 'utils/utils.interfaces';

export abstract class BaseQuery implements IQuery {
  @Expose()
  @Transform((transform) => Number(transform.value) || 1)
  page: number;

  @Expose()
  @Transform((transform) => Number(transform.value) || 20)
  pageSize: number;

  @Expose()
  createdBy: number;
}

export class BaseQueryTypeORM extends BaseQuery implements IQueryTypeORM {
  static createQueryTypeORM(query: IQuery) {
    const queryTypeORM = new this();
    Object.assign(queryTypeORM, query);
    return queryTypeORM;
  }

  /**
   * Add method [key]WithQueryBuilder:(where parameter) to custom query builder in extends model
   * @param queryBuilder
   * @returns
   */
  toQueryBuilder(queryBuilder: SelectQueryBuilder<any>) {
    Object.keys(this)
      .filter((key) => !['page', 'pageSize'].includes(key) && this[key])
      .map((key) => {
        if (typeof this[`${key}WithQueryBuilder`] === 'function') {
          queryBuilder = this[`${key}WithQueryBuilder`](queryBuilder);
        } else {
          queryBuilder = queryBuilder.andWhere(
            `${queryBuilder.alias}.${key} = :${key}`,
            {
              [key]: this[key],
            },
          );
        }
      });
    return queryBuilder.limit(this.pageSize).offset((this.page - 1) * 20);
  }
}
