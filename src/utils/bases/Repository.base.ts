import { Repository } from 'typeorm';
import { IQueryTypeORM, IRepository } from 'utils/utils.interfaces';

export abstract class BaseRepository<T>
  extends Repository<T>
  implements IRepository
{
  getQueryBuilderList(query: IQueryTypeORM, alias: string) {
    const queryBuilder = this.createQueryBuilder(alias).orderBy(
      `${alias}.updatedAt`,
      'DESC',
    );

    return query.toQueryBuilder(queryBuilder);
  }
}
