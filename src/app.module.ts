import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ResourcesModule } from 'resources/resources.module';
import { CommonModule } from './common/common.module';
import { DatabasesModule } from './databases/databases.module';

@Module({
  imports: [DatabasesModule, CommonModule, ResourcesModule],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
  ],
})
export class AppModule {}
